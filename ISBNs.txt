Note: It takes a while for Bookshelf to process the metadata and make the ISBNs available for lookup in the upload section.
---------------------------------------------------------------------------------------------------------------------------

IMPRINT. Use: "Didasko Digital Publishing" (without quotes) instead of the 16090 code (which doesn't seem to work).
-------
If your product is digital only and no print ISBN is associated this is not a required field in the metadata template and can be left blank. Your imprint / company name: Digital Publishing and Imprint ID is: 16090

------------------------------------------------------------

FOR MORE DETAILS ABOUT ISBNs, OPEN THE Didasko_metadata-20201211 EXCEL FILE.

// Shortcut:
9780645146806	[BU11] 		Business Statistics: The formula for smart business decisions
9780645146813	[IT08] 		Programming Essentials: Cracking the code - JAVA
9780645146875	[IT08]--python 	Introduction to Python: Learn the Language of Python
9780645146882	[SET01] 	Study Skills Toolkit: Your guide to university success
9780645146820	[IT06] 		IT Fundamentals: Tech foundations built from the ground up


https://success.vitalsource.com/hc/en-gb/articles/360057235434-Create-a-Library-and-Packages-in-Manage
VERY USEFUL: https://success.vitalsource.com/hc/en-gb/articles/360057306114-VitalSource-Terminology
https://success.vitalsource.com/hc/en-gb/articles/360051537894-Search-for-and-Maintain-Your-Assets-in-VitalSource-Manage

Libraries seems to be a way of distributing multiple EPUBs at once (once the Library is made into a Package).

How to get an EPUB selling on the store:
- Look up the EPUB in the search.
- Click "Pricing".
- Set "Sell on store" to true.
- Maybeeeeeeee it takes a while for it to show up in store?