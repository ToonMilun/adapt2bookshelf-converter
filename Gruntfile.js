module.exports = function(grunt){

  var path = require('path');

  require('load-grunt-config')(grunt, {
    configPath: path.join(process.cwd(), 'grunt/config'),
    jitGrunt: {
      customTasksDir: 'grunt/tasks'
    },
    data: {
      sourcedir: 'src',         // accessible with '<%= src %>'
      outputdir: 'build',
      scormdir: 'SCORM',
      libdir: 'src/lib',
      templatedir: 'src/templates',
      requiredir: 'src/require', // These files will get copied into the build directory first.
      requirelegacydir: 'src/require--legacy' // These files are also copied if "grunt build-legacy" is being used.
    }
  });
  
};