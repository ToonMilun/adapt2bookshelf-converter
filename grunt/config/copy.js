module.exports = {
  require: {
    files: [
      {
        expand: true,
        cwd: '<%= requiredir %>/',
        src: ['**'],
        dest: '<%= outputdir %>/'
      }
    ]
  },
  "require--legacy": {
    files: [
      {
        expand: true,
        cwd: '<%= requirelegacydir %>/',
        src: ['**'],
        dest: '<%= outputdir %>/'
      }
    ]
  },
  // Copy the covers from the matching ISBN folder in src/assets/*
  covers: {
    files: [
      {
        nonull: true, // Ensure the files are there.
        expand: true,
        cwd: '<%= sourcedir %>/assets/<%= isbn %>',
        src: ['cover_*.jpg'],
        dest: '<%= outputdir %>/EPUB/covers'
      }
    ]
  }
};
