module.exports = {
  zip: {
    files: [
  		{
        src: '<%= outputdir %>\\EPUB.zip',
        dest: '<%= outputdir %>\\<%= epubName %>'
      },
    ]
  }
};
