module.exports = function(grunt){
  return {
    isbn: {
      options: {
        questions: [
          {
            config: 'isbn',   // arbitrary name or config for any other grunt task
            type: 'input',    // list, checkbox, confirm, input, password
            message: 'What is the ISBN (do NOT enter dashes)? It will start with "978": ',  // Question to ask the user
            default: '978',   // default value if nothing is entered
            validate: function(value){  // return true if valid, error message if invalid. works only with type:input
              return true;
            }, 
            filter:  function(value){   // modify the answer

              if ((value + "").length < 10) {  // ISBN is too short.
                //const chalk = require('chalk');
                //grunt.log.writeln(chalk.bgRed("ERROR: ISBN provided is too short. Using a fallback value."));
                return "9780000000000";
              } 
              
              return value;
            }
          }
        ]
      }
    }
  }
};
