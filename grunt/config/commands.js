module.exports = {
  zip: {
    cmd: '<%= sourcedir %>\\zip.bat' // Run a custom Batch script to zip the contents and rename the ##mimetype file.
  },
  epubcheck: {
    cmd: 'java -jar <%= libdir %>\\epubcheck\\epubcheck.jar "<%= outputdir %>\\<%= epubName %>"'
  }
}; //https://github.com/luozhihua/grunt-commands
