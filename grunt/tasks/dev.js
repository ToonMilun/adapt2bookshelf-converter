module.exports = function(grunt) {
	// Only perform certain tasks (for testing purposes).
	grunt.registerTask('dev', [
		/*'chapterCount',
		'storeEpubData',
		'helpers',
		'build-cover',
		'build-chapters',
		'build-glossary',
		'build-resources-references',
		'build-authors',
		'build-toc',		// Create the toc.xhtml.
		'build-package',	// Create the package.opf.
		'commands:zip',		// Zip up the contents of build into EPUB.zip (using batch script).
		'rename:zip',		// Rename the EPUB.zip to "isbn - unitTitle.epub".
		'commands:epubcheck',// Check the epub.
		'outro'*/

		'helpers',		// Register some Handlebars helpers.
		'storeEpubData',
		'build-cover',
		//'commands:epubcheck'
	]);
};