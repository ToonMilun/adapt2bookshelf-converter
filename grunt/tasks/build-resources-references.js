module.exports = function(grunt) {
	grunt.registerTask('build-resources-references', 'Creates the resources-references.xhtml page. May only include one or the other type of content.', function(mode) {
		
		//let done = this.async(); // This task will be asynchronous.

		const chalk = require('chalk');
		const path = require('path');
		const handlebars = require('handlebars');
		const _ = require('underscore');

		const contentdir = path.join(grunt.config.get("outputdir"), "EPUB", "content"); // Location where to save the page.

		grunt.log.writeln(chalk.yellowBright("Will only include the category if it has items.\nIf neither category has items, the resources-references.xhtml page will NOT be created."));
		
		let references = [];
		let resources = []; // Update: Resources are now grouped by the topics they originate from.

		// Get all references and resources from all epubData.json files
		// ------------------------------------------------------------------
		grunt.config.get("epubData").forEach(data => {
			
			//grunt.log.ok(JSON.stringify(data));

			// References
			try {
				data.references.forEach(r => {
					references.push(r);
				});
			}
			catch (err) {
				grunt.fail.fatal("An unexpected error has occurred: could not get/find reference data!");
			}

			//grunt.log.ok(JSON.stringify(data.resources));

			// Resources
			let rGroup = []; 
			try {

				data.resources.forEach(r => {

					// Legacy support
					// ------------------------------------------
					// WARNING: HARDCODED FOR BU11 ONLY!
					if (process.argv.includes("build-legacy")){
						
						let href = r.href.split("/");
						href = href[href.length-1];

						r.href = "https://adaptdl.didaskogroup.com/downloads/BU11/BU11.legacy/" + href;
						r.href = encodeURI(r.href); // Ensure the URI is encoded in a way XHTML will accept (aka, special characters encoded with %).

						// Ignore duplicate titles.
						//if (resources.find(resource => {return resource.title.toLowerCase() == r.title.toLowerCase();})) return;
						rGroup.push(r);
						return;
					}

					let resource = {
						title: r.title
					};
					// EXTERNAL URLs are assigned to r.href by the Adapt epub-override.js grunt task.
					if (r.href) {
						resource.href = encodeURI(r.href);
					}
					// INTERNAL URLs need to be modified in order to link to an external mirror.
					// FIXME: The way this URL is created is a bit arbitrary...
					// And furthermore, this URL conversion is hardcoded inside of Adapt too, so if one is changes the other has to be too.
					if (r.download) {
						let download = "https://adaptdl.didaskogroup.com/downloads/";
						download += path.join(data.productionCode, data.productionCode + "." + data.topicNum, r.download);
						download = download.replace(/\\/g, "/");
						resource.download = encodeURI(download);
					}
					rGroup.push(resource);
				});
			}
			catch (err) {
				grunt.fail.fatal("An unexpected error has occurred: could not get/find resource data!");
			}

			// Group resources by their topic names.
			if (rGroup.length) {
				resources.push({
					title: data.topicTitle,
					items: rGroup
				});
			}
		});

		references = _.sortBy(references, "body");
		//resources = _.sortBy(resources, "title");

		/*if (resources) {
			resources.forEach(resource => {
				grunt.log.writeln(chalk.redBright(resource.title) + " " + chalk.bgRedBright(chalk.black(` ${resource.href || resource.download} `)));
			});
		}*/

		// Create the content/resources-references.xhtml
		// -----------------------------------------------------
		let resourcesTemplate = grunt.file.read(path.join(grunt.config.get("templatedir"), "resources.hbs"));
		let resourcesHtml = resources.length ? handlebars.compile(resourcesTemplate)(resources) : null;
		
		let referencesTemplate = grunt.file.read(path.join(grunt.config.get("templatedir"), "references.hbs"));
		let referencesHtml = references.length ? handlebars.compile(referencesTemplate)(references) : null;
		
		let pageTitle = "";
		if (resourcesHtml && referencesHtml) pageTitle = "Resources & references";
		else if (resourcesHtml) pageTitle = "Resources";
		else if (referencesHtml) pageTitle = "References";
		else return; // No resources or references; don't create the page.

		let pageTemplate = grunt.file.read(path.join(grunt.config.get("templatedir"), "page.hbs"));
		let pageHtml = handlebars.compile(pageTemplate)({
			title: pageTitle,
			items: [
				resourcesHtml,
				referencesHtml
			]
		});

		let dest = path.join(contentdir, "resources-references.xhtml");
		grunt.file.write(dest, pageHtml);

		grunt.config.addSpineItem(dest, {title: pageTitle});
	});
};