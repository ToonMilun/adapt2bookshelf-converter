module.exports = function(grunt) {
	grunt.registerTask('validate-xhtml', 'Checks all XHTML files created for the content.', function(mode) {
		
		//let done = this.async(); // This task will be asynchronous.

		const _ = require('underscore');
		const chalk = require('chalk');
		const path = require('path');
		const HtmlValidate = require('html-validate').HtmlValidate; // Check XHTML for errors after creating it.

		const htmlValidate = new HtmlValidate();
		const epubDir = path.join(grunt.config.get("outputdir"), "EPUB");
		const contentDir = path.join(grunt.config.get("outputdir"), "EPUB", "content");

		/** Check the newly generated XHTML for errors. */
		// https://html-validate.org/dev/running-in-browser.html

		let hasErrors = false;
		grunt.file.expand({filter: 'isFile'}, [path.join(epubDir, '*.xhtml'), path.join(contentDir, '*.xhtml')]).forEach(function(p) {
			
			grunt.log.writeln(chalk.yellow("Checking " + p + "..."));
			const report = htmlValidate.validateString(grunt.file.read(p));
			if (!report.valid) {
				grunt.log.writeln("\n" + chalk.bgRedBright(chalk.black("VALIDATION ERROR(S) FOUND IN XHTML: ")) + " " + chalk.redBright(p));
				_.each(report.results, (result) => {
					_.each(result.messages, (message) => {
						grunt.log.error(JSON.stringify(message, null, 4));
					});
				});

				hasErrors = true;
				grunt.log.writeln("\n");
			}
		});

		if (hasErrors) {
			grunt.fail.fatal("Please find and fix the cause of the above validation error(s) and try again.");
		}
	});
};