module.exports = function(grunt) {
	grunt.registerTask('chapterCount', 'Counts and stores the amount of chapters in this EPUB (based on the provided number of zips).', function(mode) {
		
		const chalk = require('chalk');
		const path = require('path');

		let scormdir = grunt.config.get("scormdir");

		// Record how many chapters the content has.
		let chapterCount = 0;
		grunt.file.expand({filter: 'isFile'}, path.join(scormdir, '*.zip')).forEach(function(p) {chapterCount++;});
		grunt.config.set("chapterCount", chapterCount);
		grunt.log.writeln(chalk.magentaBright("Total chapters: " + grunt.config.get("chapterCount")));
	});
};