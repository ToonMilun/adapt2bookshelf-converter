const { utimesSync } = require('fs');

module.exports = function(grunt) {
	grunt.registerTask('build-package', 'Creates the package.opf', function(mode) {
		
		const chalk = require('chalk');
		const path = require('path');
		const handlebars = require('handlebars');
		const mime = require('mime-types');
		const _ = require('underscore');

		const epubdir = path.join(grunt.config.get("outputdir"), "EPUB");


		// Read data
		// -----------------------------------------------------
		
		// Get ISBN number.
		let isbn = grunt.config.get("isbn");

		// Get data from epubData.json (Adapt's "grunt build-epub" task generates this).
		let epubDataPath = path.join(epubdir, "content/chapter1/epubData.json");
		if (!grunt.file.exists(epubDataPath)) {
			grunt.log.error("Could not find epubData.json in the .zip file (it's possible that a .zip is still extracting as this code is executing.");
			grunt.fail.fatal("Ensure you built your topic using 'grunt build-epub'.");
		}
		let epubData = grunt.file.readJSON(epubDataPath);

		let date = new Date();


		// Record each item (file) in the EPUB and its metadata
		// -----------------------------------------------------
		let i = 0;
		let items = [];
		let chapterItems = [];
		grunt.file.expand({filter: 'isFile'}, path.join(epubdir, '**/*')).forEach(function(p) {

			let _path = path.relative(epubdir, p); // Make the path relative to the location of the package.opf.
			let baseName = path.basename(_path);
		
			if (baseName == "Thumbs.db") return true; 	// Skip Thumbs.db (Windows 10 generated files).
			if (baseName == "epubData.json") return true; // We don't need to include these in the EPUB.
			if (_path == "package.opf") return true; 	// Skip the package.opf itself.

			_path = _path.replace(/\\/g, "/");

			let mimeType = mime.lookup(_path) || 'text/plain'; // Fallback
			let item = {
				href: 		encodeURI(_path),
				mediaType: 	mimeType,
				properties:	"",
				id: i
			}

			// FIXME: Paths need to be provided with /, not \

			// adapt/css/adapt.css (legacy)
			// -------------------------------
			// Legacy versions of Adapt have remote resources referenced in their CSS. Add the corresponding tag. 
			/*if (process.argv.includes('build-legacy')) {
				if (path.dirname(_path).includes("adapt/css") && baseName == "adapt.css") {
					item.properties = "remote-resources";
				}
			}*/

			// covers\cover_lg.jpg
			// -------------------------------
			if (path.dirname(_path) == "covers" && baseName == "cover_lg.jpg") {
				item.properties = "cover-image";
				item.id = "coverimage";
				i--;
			}
			// toc.xhtml
			// -------------------------------
			else if (_path == "toc.xhtml") {
				item.properties = "nav";
				item.id = "nav";
				i--;
			}
			// spineItems
			// -------------------------------
			// Ignore items which are in the global spineItems array.
			else if (_.find(grunt.config.get("spineItems"), (item) => {return item.href == _path})) {
				return true;
			}

			items.push(item);
			//grunt.log.writeln(chalk.whiteBright(i) + chalk.white(" --- " + _path + " --- ") + chalk.cyanBright(mimeType));
			
			i++;
		});



		// Create the package.opf
		// -----------------------------------------------------
		let packageTemplate = grunt.file.read(path.join(grunt.config.get("templatedir"), "package.hbs"));

		let package = handlebars.compile(packageTemplate)({
			isbn: 			isbn,
			unitTitle:		epubData.unitTitle,
			publishedDate:	grunt.template.date(date, "yyyy-mm-dd"), // TEMP?
			modifiedDate:	grunt.template.date(date, "yyyy-mm-dd") + "T" + grunt.template.date(date, "hh:MM:ss") + "Z",
			authors:		epubData.authors,
			copyrightYear:	grunt.template.date(date, "yyyy"),
			items:			items,
			spineItems:		grunt.config.getSpineItems()
		});
		grunt.file.write(path.join(epubdir, "package.opf"), package);
	});
};