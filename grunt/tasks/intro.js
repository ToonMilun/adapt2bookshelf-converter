module.exports = function(grunt) {
	grunt.registerTask('intro', 'Print intro messages', function(mode) {
		
		const chalk = require('chalk');

		grunt.log.writeln(chalk.bgMagenta("\n---------- Adapt2Bookshelf ----------"));
		
		grunt.log.writeln(chalk.yellowBright("\nWARNING! This script will process MANY files once it's started. Please MAKE SURE you've read the README.txt and understand what this will do. If you are unsure, please close this program IMMEDIATELY!\n- Milton Plotkin"));

	});
};