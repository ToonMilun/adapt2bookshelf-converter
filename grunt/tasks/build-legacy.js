module.exports = function(grunt) {
	grunt.registerTask('build-legacy', [
		'intro',			
		'clean:output',		
		'prompt',			
		'copy:require',		
		'copy:require--legacy',		// Copy additional files required for legacy builds.
		'copy:covers',
		'chapterCount',		
		'extract',
		'storeEpubData',	
		
		// Build supplementary pages 
		'helpers',
		'build-cover',
		'build-chapters',
		'build-glossary',
		'build-resources-references',
		'build-toc',
		'build-package',
		// --------------------------
		
		'commands:zip',
		'rename:zip',
		'commands:epubcheck',
		'outro'
	]);
};