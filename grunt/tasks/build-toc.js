const { utimesSync } = require('fs');

module.exports = function(grunt) {
	grunt.registerTask('build-toc', 'Creates the toc.xhtml', function(mode) {
		
		const chalk = require('chalk');
		const path = require('path');
		const handlebars = require('handlebars');
		const _ = require('underscore');

		const epubdir = path.join(grunt.config.get("outputdir"), "EPUB");

		// Get epubData for each chapter.
		// -----------------------------------------------------
		let chapterItems = [];

		let spineItems = grunt.config.getSpineItems();

		// Read in block data for the chapter items.
		spineItems.forEach((e, i) => {

			if (e.chapterNum === undefined) return;

			let epubData = grunt.file.readJSON(path.join(epubdir, 'content/chapter' + e.chapterNum + '/epubData.json'));
			epubData.blocks.forEach((e, i) => {
				e.blockNum = i+1;
			});

			e.topicTitle = epubData.topicTitle;
			e.blocks = epubData.blocks;
		});

		// Debug log.
		// -----------------------------------------------------
		grunt.log.writeln(chalk.yellowBright("Chapters:"));
		spineItems.forEach((item, i) => {
			grunt.log.writeln(chalk.whiteBright((i) + " - ") + chalk.greenBright(item.title));
		});

		// Create the toc.xhtml
		// -----------------------------------------------------
		let tocTemplate = grunt.file.read(path.join(grunt.config.get("templatedir"), "toc.hbs"));
		let toc = handlebars.compile(tocTemplate)({
			unitTitle: 	grunt.config.get("unitTitle"),
			spineItems: spineItems
		});
		grunt.file.write(path.join(epubdir, "toc.xhtml"), toc);

		return;







		/*
		grunt.file.expand({filter: 'isFile'}, path.join(epubdir, 'content/chapter/epubData.json')).forEach(function(p) {
			
			let chapterNum = p.match(/[\\\/]chapter(\d+)[\\\/]/)[1]; // Extract the correct chapter number from the path itself.
			chapterNum = parseInt(chapterNum);
			let epubData = grunt.file.readJSON(p);
			epubData.blocks.forEach((e, i) => {
				e.blockNum = i+1;
			});

			chapterItems[chapterNum-1] = {
				unitTitle: epubData.unitTitle,
				chapterNum: chapterNum,
				blocks: epubData.blocks
			}
			grunt.log.writeln(chalk.greenBright("chapter" + chapterNum));
		});

		// Check that the amount of chapters processed = the amount of chapters there are.
		if (grunt.config.get("chapterCount") !== chapterItems.length) {
			grunt.fail.fatal("An unexpected error has occurred: could not find an epubData.json for each chapter!");
		}

		// Create the toc.xhtml
		// -----------------------------------------------------
		let tocTemplate = grunt.file.read(path.join(grunt.config.get("templatedir"), "toc.hbs"));
		let toc = handlebars.compile(tocTemplate)({
			unitTitle: 	grunt.config.get("unitTitle"),
			chapterItems: chapterItems
		});
		grunt.file.write(path.join(epubdir, "toc.xhtml"), toc);
		*/



	});
};