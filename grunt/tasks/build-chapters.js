const { utimesSync } = require('fs');

module.exports = function(grunt) {
	grunt.registerTask('build-chapters', 'Finds and records the paths of all the chapter*/index.xhtml.', function(mode) {
		
		const chalk = require('chalk');
		const path = require('path');

		const contentdir = path.join(grunt.config.get("outputdir"), "EPUB", "content"); // Location where to save the authors page.
		let epubData = grunt.config.get("epubData");

		// Find and record the paths of all the chapter*/index.xhtml files
		// ---------------------------------------------------------------
		// Generate using the length of epubData.
		for (let i = 0; i < epubData.length; i++) {
			
			let chapterNum = i+1;
			
			let p = path.join(contentdir, 'chapter' + chapterNum + '/index.xhtml');
			grunt.config.addSpineItem(p, {
				properties: "scripted",
				chapterNum: chapterNum,
				title: epubData[i].topicTitle
			});
		}
	});
};