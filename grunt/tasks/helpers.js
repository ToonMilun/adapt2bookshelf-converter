module.exports = function(grunt) {
	grunt.registerTask('helpers', 'Register Handlebars and Grunt helpers.', function(mode) {
		
		const handlebars = require('handlebars');
		const entities = require('special-entities'); // Used to auto convert characters that are invalid in XHTML.

		handlebars.registerHelper("compile", function(template, context) {
			if (!template) {
			  return '';
			}
			if (template instanceof Object) template = template.toString();
			let data = this;
			if (context) {
			  // choose between a passed argument context or the default handlebars helper context
			  //data = (context.data?.root ?? context);
			  data = context.data.root;
			}
			return handlebars.compile(template)(data);
		});

		handlebars.registerHelper("normalize", function() {
			let context = arguments[arguments.length-1];
			let html = context.fn(this);

			// Escape any "&" characters (that aren't a part of a HTML entity).
			html = html.replace(/(&)(\s|\S[^;]*?\s)/g, (match, $1, $2) => {return "&amp;" + $2;});

			return new handlebars.SafeString(entities.normalizeXML(html, 'numeric'));
		});

		// Need to register #hyperlink helper for references.
		handlebars.registerHelper("hyperlink", function() {
	
			let context = arguments[arguments.length-1];

			let href = context.hash.href || context.fn(this);

			let html = '<a class="hyperlink" href="' + href + '" target="_blank">' + context.fn(this) + '</a>';
			return new handlebars.SafeString(html);
		});

		// Converts all \ to / in a path (EPUB requirement).
		handlebars.registerHelper("path", function() {
	
			let context = arguments[arguments.length-1];
			return context.fn(this).replace(/\\/g, "/");
		});
	});
};