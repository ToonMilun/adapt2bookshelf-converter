module.exports = function(grunt) {
	grunt.registerTask('outro', 'Prints the outro messages.', function(mode) {
		
		const chalk = require('chalk');
		grunt.log.writeln(chalk.bgRed("\nIMPORTANT: Fix all epubcheck 'ERROR' messages before uploading!"));
	});
};