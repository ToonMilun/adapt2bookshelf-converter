module.exports = function(grunt) {
	grunt.registerTask('build-glossary', 'Creates the glossary.xhtml page.', function(mode) {
		
		const chalk = require('chalk');
		const path = require('path');
		const handlebars = require('handlebars');
		const _ = require('underscore');

		const contentdir = path.join(grunt.config.get("outputdir"), "EPUB", "content"); // Location where to save the page.

		grunt.log.writeln(chalk.yellowBright("This will combine all glossaries from all topics in alphabetical order."));
		
		let glossaries = [];

		// Get all glossaries from all epubData.json files
		// ------------------------------------------------------------------
		grunt.config.get("epubData").forEach(data => {
			try {
				data.glossaries.forEach(g => {
					glossaries.push(g);
				});
			}
			catch (err) {
				grunt.log.fatal("An unexpected error has occurred: could not get/find glossary data! (Topic: " + data.topicTitle + ").");
			}
		});

		// If no glossaries, don't add the page.
		if (!glossaries.length) {
			grunt.log.writeln(chalk.redBright("No glossaries found in any of the topics!"));
			grunt.log.writeln(chalk.redBright("glossaries.xhtml was NOT created."));
			return;
		}

		// Print all glossaries
		// ------------------------------------------------------------------
		glossaries.sort((a, b) => {
			return a.title.localeCompare(b.title);
		});
		glossaries.forEach((glossary, i) => {
			grunt.log.writeln(chalk.whiteBright(i) + " - " + chalk.greenBright(glossary.title));
		});

		// Merge glossaries with the same title
		// ------------------------------------------------------------------
		let g = {};
		glossaries.forEach((glossary) => {
			g[glossary.title] = glossary;
		});
		if (Object.keys(g).length < glossaries.length) {
			grunt.log.errorlns("One or more duplicate glossaries detected; the latest definition for these has been used.");
		}
		glossaries = [];
		_.each(g, e => {glossaries.push(e);});

		// Create the content/glossary.xhtml
		// -----------------------------------------------------
		let glossaryTemplate = grunt.file.read(path.join(grunt.config.get("templatedir"), "glossary.hbs"));
		let glossaryHtml = handlebars.compile(glossaryTemplate)(glossaries);

		let pageTemplate = grunt.file.read(path.join(grunt.config.get("templatedir"), "page.hbs"));
		let pageHtml = handlebars.compile(pageTemplate)({
			title: "Glossary",
			items: [
				glossaryHtml
			]
		});

		let dest = path.join(contentdir, "glossary.xhtml");
		grunt.file.write(dest, pageHtml);

		grunt.config.addSpineItem(dest, {title: "Glossary"});
	});
};