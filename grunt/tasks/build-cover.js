const { utimesSync } = require('fs');

module.exports = function(grunt) {
	grunt.registerTask('build-cover', 'Creates the cover.xhtml page.', function(mode) {
		
		const chalk = require('chalk');
		const path = require('path');
		const handlebars = require('handlebars');
		const _ = require('underscore');

		const contentdir = path.join(grunt.config.get("outputdir"), "EPUB", "content"); // Location where to save the authors page.

		// Get a unique list of credits from all epubData.json files.
		// ------------------------------------------------------------------
		grunt.log.writeln(chalk.yellowBright("Warning: this will combine all \"_credits\" from all topics into one list. The order may be affected.\n"));
		
		let credits = {}; // key: role, value: unique array of names.
		
		grunt.config.get("epubData").forEach(data => {
			(data.credits || []).forEach(c => {
				if (credits[c.role] === undefined) credits[c.role] = [];
				credits[c.role] = credits[c.role].concat(c.names);
			});
		});
		// Check and remove any duplicate names (per-role).
		_.each(credits, (names, role) => {
			credits[role] = _.unique(names);
		});

		// Change dictionary back into array.
		let _credits = [];

		// Log the data.
		grunt.log.writeln(chalk.bgGreen("Credits:"));
		_.each(credits, (names, role) => {

			_credits.push({
				role: role,
				names: names
			});

			grunt.log.writeln(chalk.greenBright(role));
			names.forEach((name) => {
				grunt.log.writeln("- " + chalk.whiteBright(name));
			});
		});

		if (!_credits.length) {
			grunt.log.writeln("- " + chalk.bgRedBright("ERROR: Could not find \"_credits\" property defined in any of the Adapt Topic's contentObjects.json files! Please add it and try again."));
			return;
		}

		// Create the content/cover.xhtml
		// -----------------------------------------------------
		let coverTemplate = grunt.file.read(path.join(grunt.config.get("templatedir"), "cover.hbs"));
		let coverHtml = handlebars.compile(coverTemplate)({
			unitTitle: grunt.config.get("unitTitle"),
			year: grunt.template.today("yyyy"),
			credits: _credits
		});

		let pageTemplate = grunt.file.read(path.join(grunt.config.get("templatedir"), "page.hbs"));
		let pageHtml = handlebars.compile(pageTemplate)({
			title: "Cover",
			items: [
				coverHtml
			]
		});

		let dest = path.join(contentdir, "cover.xhtml");
		grunt.file.write(dest, pageHtml);

		grunt.config.addSpineItem(dest, {title: "Cover"});
	});
};