module.exports = function(grunt) {
	grunt.registerTask('extract', 'Extracts all .zips from inside the SCORM directory into build/EPUB/ directory.', function(mode) {
		
		let done = this.async(); // This task will be asynchronous.

		const chalk = require('chalk');
		const path = require('path');
		const seven = require('node-7z');

		let scormdir = grunt.config.get("scormdir");
		let contentdir = path.join(grunt.config.get("outputdir"), "EPUB/content"); // Extract all zips to "chapter*" directories here.
		
		let chapterNum = 1; // Increments.
		let chapterCount = grunt.config.get("chapterCount");

		// For each .zip in the scorm dir.
		// Note: assumes the builds are in alphabetical order.
		let chaptersExtracted = 0;
		grunt.log.writeln(chalk.bgMagenta("Extracting:"));
		grunt.log.writeln(chalk.magenta("(If you encounter: 'Fatal error: spawn 7z ENOENT', you'll need to add '7z' to your Windows 'PATH')."));

		let paths = grunt.file.expand({filter: 'isFile'}, path.join(scormdir, '*.zip'));
		let badNames = false;

		// Sort the paths exactly as they show up in Windows explorer (otherwise, you get [1, 10, 11, 2, etc.]).
		paths.sort((a, b) => {

			let aName = path.basename(a, ".zip");
			let bName = path.basename(b, ".zip");

			// Filename formats supported:
			// IT06.5
			// IT06_T5
			// IT06.5-1
			// IT06_T5-1
			let aNum = aName.match(/[\d-]+$/g);
			let bNum = bName.match(/[\d-]+$/g);

			// If numbers were found at the end of the filenames (which should be the case):
			if (aNum && bNum) {
				aNum = aNum[0];
				bNum = bNum[0];
				
				// Replace any "-" with ".".
				aNum = aNum.replace(/-/g, ".");
				bNum = bNum.replace(/-/g, ".");

				// Parse to real numbers.
				aNum = parseFloat(aNum);
				bNum = parseFloat(bNum);

				// Sort by those numbers.
				return aNum - bNum; 
			}

			// Fallback: if the filenames do not end in numbers, sort by string comparisson instead.
			badNames = true;
			return (aName + "").localeCompare((bName + ""));
		});

		if (badNames) {
			grunt.log.writeln(chalk.brightRed("Bad naming conventions detected for one or more files! This may lead to unpredictable results!\n\nPlease ensure that each of your .zip filenames ends in a number!"));
		}

		paths.forEach(function(p) {

			let chapterdir = path.join(contentdir, "chapter" + chapterNum);
			
			// Extract the content of the SCORM folder.
			let myStream = seven.extractFull(p, chapterdir, {
				$progress: true
			});
			grunt.log.writeln("Extracting: " + chalk.yellowBright(p) + " ---> " + chalk.greenBright(chapterdir));
			myStream.on('end', function() {
				chaptersExtracted++;
				if (chaptersExtracted == chapterCount) {
					grunt.log.writeln(chalk.bgGreen("All zips extracted!"));
					done();
				}
			});

			chapterNum++;
		});
	});
};