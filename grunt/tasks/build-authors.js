const { utimesSync } = require('fs');

module.exports = function(grunt) {
	grunt.registerTask('build-authors', 'Creates the authors.xhtml page.', function(mode) {
		
		const chalk = require('chalk');
		const path = require('path');
		const handlebars = require('handlebars');
		const _ = require('underscore');

		const contentdir = path.join(grunt.config.get("outputdir"), "EPUB", "content"); // Location where to save the authors page.

		// Get a unique list of authors from all epubData.json files.
		// ------------------------------------------------------------------
		grunt.log.writeln(chalk.yellowBright("Warning: this will combine all authors from all topics into one list. The order may be affected."));
		
		let authorNames = [];
		
		grunt.config.get("epubData").forEach(data => {
			data.authors.forEach(author => {
				authorNames.push(author.name);
			});
		});
		authorNames = _.unique(authorNames);
		
		authorNames.forEach((name, i) => {
			grunt.log.writeln(chalk.whiteBright(i) + " - " + chalk.greenBright(name));
		});
		
		
		// Get the author's description from EPUB/content/chapter1/assets/authors/authors.json
		// ------------------------------------------------------------------
		let authorsData = grunt.file.readJSON(path.join(contentdir, "chapter1/assets/authors/authors.json"));
		let authors = [];

		authorNames.forEach((name, i) => {
			let author = _.find(authorsData, (a) => {return a.name == name});
			if (!author) grunt.fail.fatal("ERROR: Could not find data for '" + name + "'!");
			// Don't include hidden authors.
			if (author.hidden) {
				grunt.log.writeln(chalk.yellowBright("'" + name + "' has been marked as 'hidden' in authors.json, and will not be included."));
				return true;
			}
			// Don't include hidden authors.
			if (author.body.trim().length == 0) {
				grunt.log.writeln(chalk.bgRed("ERROR: '" + name + "' does not have a defined \"body\" property."));
			}

			authors.push(author);
		});

		// Create the content/authors.xhtml
		// -----------------------------------------------------
		let authorsTemplate = grunt.file.read(path.join(grunt.config.get("templatedir"), "authors.hbs"));
		let authorsHtml = handlebars.compile(authorsTemplate)(authors);

		let pageTemplate = grunt.file.read(path.join(grunt.config.get("templatedir"), "page.hbs"));
		let pageHtml = handlebars.compile(pageTemplate)({
			title: "Authors",
			items: [
				authorsHtml
			]
		});

		let dest = path.join(contentdir, "authors.xhtml");
		grunt.file.write(dest, pageHtml);

		grunt.config.addSpineItem(dest, {title: "Authors"});
	});
};