module.exports = function(grunt) {
	grunt.registerTask('storeEpubData', 'Reads the epubData.json file from each chapter and stores them.', function(mode) {
		
		const chalk = require('chalk');
		const path = require('path');
		const epubCheck = require('epubCheck');
		const _ = require('underscore');

		const epubdir = path.join(grunt.config.get("outputdir"), "EPUB");

		// Get epubData from all chapters
		// ------------------------------------------------------------------
		let epubData = [];

		grunt.file.expand({filter: 'isFile'}, path.join(epubdir, 'content/chapter*/epubData.json')).forEach(function(p) {
			
			let chapterNum = p.match(/[\\\/]chapter(\d+)[\\\/]/)[1]; // Extract the correct chapter number from the path itself.
			chapterNum = parseInt(chapterNum);

			let data = grunt.file.readJSON(p);
			data.chapterNum = chapterNum;

			// Add blockNums to each block in the data.
			data.blocks.forEach((e, i) => {
				e.blockNum = i+1;
			});

			epubData[chapterNum-1] = data;
		});

		// Store it.
		grunt.config.set("epubData", epubData);

		// Store the unitTitle.
		grunt.config.set("unitTitle", epubData[0].unitTitle);
		grunt.config.set("productionCode", epubData[0].productionCode);

		// Also, store the name of the .epub that will be created.
		let epubName = (grunt.config.get("isbn") || "9780000000000") + " " + grunt.config.get("productionCode") + " " + grunt.config.get("unitTitle");
		
		// Make filename safe.
		epubName = epubName.replace(/[^a-z0-9]/gi, '_'); 
	
		// Ensure the filename isn't too long (Desktop Bookshelf will reject long filenames if side-loading).
		epubName = epubName.substr(0, 30) + ".epub";

		grunt.config.set("epubName", epubName);

		// Create an array that will store a list of all pages that will be in the TOC.
		// (Used later by build-package.js and build-toc.js).
		grunt.config.set("spineItems", []);
		grunt.config.getSpineItems = () => {return grunt.config.get("spineItems");}
		grunt.config.addSpineItem = (itemPath, options) => {
			let spineItems = grunt.config.get("spineItems");
			let p = path.relative(epubdir, itemPath); // Make the path relative to the location of the package.opf.
			
			// Replace all \ with /.
			p = p.replace(/\\/g, "/");
			
			spineItems.push(_.extend({href: p}, options));

			grunt.log.writeln(chalk.bgCyan("Added chapter item:") + chalk.cyanBright(" " + p));
			grunt.config.set("spineItems", spineItems);
		};

		grunt.log.writeln(chalk.greenBright("EPUB name: ") + chalk.yellowBright(grunt.config.get("epubName")));
	});
};