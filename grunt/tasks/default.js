module.exports = function(grunt) {
	grunt.registerTask('default', [
		'intro',			// Show the intro warnings.
		'clean:output',		// Clear the build folder.
		'prompt',			// Prompt the user for the ISBN number.
		'copy:require',		// Copy the files required for the EPUB.
		'copy:covers',		// Copy the cover images required for the EPUB.
		'chapterCount',		// Count how many chapters this EPUB will have.
		'extract',			// Extract all .zips in SCORM folder to the build folder using 7zip.
		'storeEpubData',	// Store the data from all the epubData.json files in the chapter* directories.
		
		// Build supplementary pages 
		'helpers',		// Register some Handlebars helpers.
		'build-cover',
		'build-chapters',
		'build-glossary',
		'build-resources-references',
		//'build-authors',	// No longer used. Authors are now included in the cover page under "Acknowledgements".
		'build-toc',		// Create the toc.xhtml.
		'build-package',	// Create the package.opf.
		'validate-xhtml',	// Validate all XHTML files that were just created.
		// --------------------------
		
		'commands:zip',		// Zip up the contents of build into EPUB.zip (using batch script).
		'rename:zip',		// Rename the EPUB.zip to "isbn - unitTitle.epub".
		'commands:epubcheck',// Check the epub.
		'outro'
	]);
};