@echo off
setlocal enableextensions enabledelayedexpansion

REM There seems to be no way of doing this from within Grunt.
REM Perform the zipping and renaming of mimetype step using this Batch script.

cd build
7z a -r EPUB.zip * m=Deflate -x^^!Thumbs.db -x^^!epubData.json
7z rn EPUB.zip ##mimetype mimetype

EXIT /B 0
EndLocal
ECHO ON